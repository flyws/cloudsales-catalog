# Cloudsales Catalog Service

## Additional commands

### Gradle Release Plugin

Gradle release execution
```bash
gradle release
```

Gradle release execution for Continuous Integration
```bash
gradle release -Prelease.useAutomaticVersion=true -Prelease.releaseVersion=1.0.0 -Prelease.newVersion=1.1.0-SNAPSHOT
```
