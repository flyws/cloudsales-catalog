package com.flyws.cloudsales.catalog.controllers;

import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.services.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {
    private static final String CATEGORY = "category";
    private static final int PAGE = 0;
    private static final int SIZE = 8;

    @InjectMocks
    private ProductController testedEntry;

    @Mock
    private ProductService productService;

    @Mock
    private List<Product> productList;

    @Test
    public void shouldReturnProductsByCategory() {
        doReturn(productList).when(productService).getProductsByCategory(CATEGORY, PAGE, SIZE);
        assertThat(testedEntry.getProductsByCategory(CATEGORY, PAGE, SIZE)).isSameAs(productList);
    }

}