package com.flyws.cloudsales.catalog.controllers;

import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.services.SearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class SearchControllerTest {
    private static final String QUERY = "query";
    private static final int PAGE = 0;
    private static final int SIZE = 5;

    @InjectMocks
    private SearchController testedEntry;

    @Mock
    private SearchService searchService;

    @Mock
    private List<Product> productList;

    @Test
    public void shouldFindProductsByQuery() {
        doReturn(productList).when(searchService).findProducts(QUERY, PAGE, SIZE);
        assertThat(testedEntry.findProducts(QUERY, PAGE, SIZE)).isSameAs(productList);
    }

}