package com.flyws.cloudsales.catalog.controllers;

import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.services.LandingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class LandingControllerTest {
    private static final int PAGE = 0;
    private static final int SIZE = 4;

    @InjectMocks
    private LandingController testedEntry;

    @Mock
    private LandingService productService;

    @Mock
    private List<Product> productList;

    @Test
    public void shouldReturnLandingProducts() {
        doReturn(productList).when(productService).getLandingProducts(PAGE, SIZE);
        assertThat(testedEntry.getLandingProducts(PAGE, SIZE)).isSameAs(productList);
    }

}