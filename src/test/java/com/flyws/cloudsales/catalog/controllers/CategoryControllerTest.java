package com.flyws.cloudsales.catalog.controllers;

import com.flyws.cloudsales.catalog.domain.Category;
import com.flyws.cloudsales.catalog.services.CategoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class CategoryControllerTest {

    @InjectMocks
    private CategoryController testedEntry;

    @Mock
    private CategoryService categoryService;

    @Mock
    private List<Category> categoryList;

    @Test
    public void shouldReturnTopCategories() {
        doReturn(categoryList).when(categoryService).getTopCategories();
        assertThat(testedEntry.getTopCategories()).isSameAs(categoryList);
    }

}