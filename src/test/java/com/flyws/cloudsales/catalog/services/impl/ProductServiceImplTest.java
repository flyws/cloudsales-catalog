package com.flyws.cloudsales.catalog.services.impl;

import com.flyws.cloudsales.catalog.domain.Category;
import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.repositories.CategoryRepository;
import com.flyws.cloudsales.catalog.repositories.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.springframework.data.domain.PageRequest.of;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {
    private static final String CATEGORY_CODE = "category";
    private static final int PAGE = 0;
    private static final int SIZE = 10;

    @InjectMocks
    private ProductServiceImpl testedEntry;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private CategoryRepository categoryRepository;

    private Category category = new Category();

    private Product product = new Product();

    @Before
    public void setUp() {
        doReturn(category).when(categoryRepository).findDistinctByCode(CATEGORY_CODE);
    }

    @Test
    public void shouldReturnProductsByCategory() {
        doReturn(singletonList(product)).when(productRepository)
                .findByCategoryIn(singletonList(category), of(PAGE, SIZE));
        assertThat(testedEntry.getProductsByCategory(CATEGORY_CODE, PAGE, SIZE)).containsOnly(product);
    }

    @Test
    public void shouldReturnProductsByCategoryAndSubcategory() {
        Category subcategory = new Category();
        Product subcategoryProduct = new Product();
        category.setSubcategories(singletonList(subcategory));
        doReturn(asList(product, subcategoryProduct)).when(productRepository)
                .findByCategoryIn(asList(category, subcategory), of(PAGE, SIZE));
        assertThat(testedEntry.getProductsByCategory(CATEGORY_CODE, PAGE, SIZE))
                .containsOnly(product, subcategoryProduct);
    }
}