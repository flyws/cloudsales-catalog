package com.flyws.cloudsales.catalog.services.impl;

import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.repositories.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.springframework.data.domain.PageRequest.*;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceImplTest {
    private static final String QUERY = "query";
    private static final int PAGE = 1;
    private static final int SIZE = 2;

    @InjectMocks
    private SearchServiceImpl testedEntry;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private List<Product> productList;

    @Test
    public void shouldFindProductsByQuery() {
        doReturn(productList).when(productRepository).findByTitleContainingIgnoreCase(QUERY, of(PAGE, SIZE));
        assertThat(testedEntry.findProducts(QUERY, PAGE, SIZE)).isSameAs(productList);
    }
}