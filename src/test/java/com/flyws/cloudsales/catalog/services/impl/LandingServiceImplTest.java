package com.flyws.cloudsales.catalog.services.impl;

import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.repositories.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.springframework.data.domain.PageRequest.of;

@RunWith(MockitoJUnitRunner.class)
public class LandingServiceImplTest {
    private static final int PAGE = 0;
    private static final int SIZE = 4;

    @InjectMocks
    private LandingServiceImpl testedEntry;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private List<Product> productList;

    @Mock
    private Page<Product> productPage;

    @Test
    public void shouldReturnLandingProducts() {
        doReturn(productPage).when(productRepository).findAll(of(PAGE, SIZE));
        doReturn(productList).when(productPage).getContent();
        assertThat(testedEntry.getLandingProducts(PAGE, SIZE)).isSameAs(productList);
    }

}