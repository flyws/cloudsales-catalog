package com.flyws.cloudsales.catalog.services.impl;

import com.flyws.cloudsales.catalog.domain.Category;
import com.flyws.cloudsales.catalog.repositories.CategoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceImplTest {

    @InjectMocks
    private CategoryServiceImpl testedEntry;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private List<Category> categoryList;

    @Test
    public void shouldReturnTopCategories() {
        doReturn(categoryList).when(categoryRepository).findByParentIsNull();
        assertThat(testedEntry.getTopCategories()).isSameAs(categoryList);
    }

}