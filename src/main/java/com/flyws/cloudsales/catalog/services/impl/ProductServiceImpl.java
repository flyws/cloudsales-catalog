package com.flyws.cloudsales.catalog.services.impl;

import com.flyws.cloudsales.catalog.domain.Category;
import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.repositories.CategoryRepository;
import com.flyws.cloudsales.catalog.repositories.ProductRepository;
import com.flyws.cloudsales.catalog.services.ProductService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.domain.PageRequest.*;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Product> getProductsByCategory(String categoryCode, int page, int size) {
        Category category = categoryRepository.findDistinctByCode(categoryCode);
        List<Category> categories = getCategoriesChain(category);
        return productRepository.findByCategoryIn(categories, of(page, size));
    }

    private List<Category> getCategoriesChain(Category category) {
        List<Category> categories = new ArrayList<>();
        categories.add(category);
        List<Category> subCategories = category.getSubcategories();
        if (CollectionUtils.isNotEmpty(subCategories)) {
            subCategories.forEach(subCategory -> categories.addAll(getCategoriesChain(subCategory)));
        }
        return categories;
    }
}
