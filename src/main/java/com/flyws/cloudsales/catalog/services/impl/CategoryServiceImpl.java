package com.flyws.cloudsales.catalog.services.impl;

import com.flyws.cloudsales.catalog.domain.Category;
import com.flyws.cloudsales.catalog.repositories.CategoryRepository;
import com.flyws.cloudsales.catalog.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getTopCategories() {
        return categoryRepository.findByParentIsNull();
    }
}
