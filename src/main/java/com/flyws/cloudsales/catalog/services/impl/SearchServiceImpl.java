package com.flyws.cloudsales.catalog.services.impl;

import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.repositories.ProductRepository;
import com.flyws.cloudsales.catalog.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

import static org.springframework.data.domain.PageRequest.of;

@Controller
public class SearchServiceImpl implements SearchService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> findProducts(String query, int page, int size) {
        return productRepository.findByTitleContainingIgnoreCase(query, of(page, size));
    }
}
