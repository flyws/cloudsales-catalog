package com.flyws.cloudsales.catalog.services;

import com.flyws.cloudsales.catalog.domain.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getTopCategories();
}
