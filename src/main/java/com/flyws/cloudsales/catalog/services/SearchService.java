package com.flyws.cloudsales.catalog.services;

import com.flyws.cloudsales.catalog.domain.Product;

import java.util.List;

public interface SearchService {
    List<Product> findProducts(String query, int page, int size);
}
