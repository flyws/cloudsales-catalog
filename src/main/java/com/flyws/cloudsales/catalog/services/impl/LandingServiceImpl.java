package com.flyws.cloudsales.catalog.services.impl;

import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.repositories.ProductRepository;
import com.flyws.cloudsales.catalog.services.LandingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.data.domain.PageRequest.of;

@Service
public class LandingServiceImpl implements LandingService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getLandingProducts(int page, int size) {
        Page<Product> productPage = productRepository.findAll(of(page, size));
        return productPage.getContent();
    }
}
