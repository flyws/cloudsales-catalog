package com.flyws.cloudsales.catalog.services;

import com.flyws.cloudsales.catalog.domain.Product;

import java.util.List;

public interface ProductService {
    List<Product> getProductsByCategory(String categoryCode, int page, int size);
}
