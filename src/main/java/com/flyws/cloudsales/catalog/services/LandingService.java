package com.flyws.cloudsales.catalog.services;

import com.flyws.cloudsales.catalog.domain.Product;

import java.util.List;

public interface LandingService {
    List<Product> getLandingProducts(int page, int size);
}
