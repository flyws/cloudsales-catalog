package com.flyws.cloudsales.catalog.repositories;

import com.flyws.cloudsales.catalog.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findByParentIsNull();
    Category findDistinctByCode(String code);
}
