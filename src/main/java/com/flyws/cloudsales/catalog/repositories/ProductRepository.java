package com.flyws.cloudsales.catalog.repositories;

import com.flyws.cloudsales.catalog.domain.Category;
import com.flyws.cloudsales.catalog.domain.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByTitleContainingIgnoreCase(String title, Pageable pageable);
    List<Product> findByCategoryIn(List<Category> categories, Pageable pageable);
}
