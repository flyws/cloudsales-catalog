package com.flyws.cloudsales.catalog.domain;

import com.fasterxml.jackson.annotation.JsonView;
import com.flyws.cloudsales.catalog.utils.json.View;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product")
public class Product {

    @JsonView(View.Public.class)
    @Id
    private Long id;

    @JsonView(View.Public.class)
    private String code;

    @JsonView(View.Public.class)
    private String title;

    @JsonView(View.Public.class)
    @ManyToOne
    @JoinColumn(name="category")
    private Category category;

    @JsonView(View.Public.class)
    private String image;

    @JsonView(View.Public.class)
    private Long price;

    @JsonView(View.Public.class)
    private String currency;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
