package com.flyws.cloudsales.catalog.utils.json;

public class View {
    public interface Public {
    }

    public interface WithSubCategories extends Public {
    }
}
