package com.flyws.cloudsales.catalog.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.services.ProductService;
import com.flyws.cloudsales.catalog.utils.json.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @JsonView(View.Public.class)
    @GetMapping(params = "categoryCode")
    public List<Product> getProductsByCategory(@RequestParam String categoryCode,
                                               @RequestParam int page,
                                               @RequestParam int size) {
        return productService.getProductsByCategory(categoryCode, page, size);
    }

}
