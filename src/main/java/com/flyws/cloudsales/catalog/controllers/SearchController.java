package com.flyws.cloudsales.catalog.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.flyws.cloudsales.catalog.domain.Product;
import com.flyws.cloudsales.catalog.services.SearchService;
import com.flyws.cloudsales.catalog.utils.json.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @JsonView(View.Public.class)
    @GetMapping("/products")
    public List<Product> findProducts(@RequestParam String query, @RequestParam int page, @RequestParam int size) {
        return searchService.findProducts(query, page, size);
    }
}
