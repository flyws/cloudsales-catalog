package com.flyws.cloudsales.catalog.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.flyws.cloudsales.catalog.domain.Category;
import com.flyws.cloudsales.catalog.services.CategoryService;
import com.flyws.cloudsales.catalog.utils.json.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @JsonView(View.WithSubCategories.class)
    @GetMapping(params = "top")
    public List<Category> getTopCategories() {
        return categoryService.getTopCategories();
    }
}
